# README #

Gitian build of BitcoinXT Only-BigBlocks branch (which is Bitcoin Core with just the BIP101 big blocks patch), with user agent string /Satoshi + BIP101:0.11.0/

* Version string and copyright string updated: https://bitbucket.org/bitcartel/bitcoinxt/commits/all
* Gitian signature
* Commit 4de91e7 - tag v0.11.0-BIP101

### Linux Binaries ###

https://bitbucket.org/bitcartel/bitcoinxt/downloads

### SHA256 of binaries ###

f25d6d2d81e69457b79d9c092d21ec196dd550480dda10ff759cd169d64cbde0  bitcoin-0.11.0-linux32.tar.gz
054efbc3eb6cdf4f2ec56e4d90c8bd2be07ed8439afcc3f419ec77dcfac5c27a  bitcoin-0.11.0-linux64.tar.gz
133011d35b48eafdfd0daf1af1146e4cbc18793dab3aac25eb0f99fb75def9cb  src/bitcoin-0.11.0.tar.gz
6183da8a05341741466d9da3e1b1bf4037180a90da197e5ce6e03a0837cfdc8f  bitcoin-linux-0.11-res.yml

### Gitian info links ###

* https://github.com/devrandom/gitian-builder
* https://github.com/bitcoin/bitcoin/blob/master/doc/gitian-building.md
* https://github.com/bitcoin/bitcoin/blob/master/doc/release-process.md

### How to build with Gitian ###

Steps to create a deterministic build of BitcoinXT using Gitian and KVM.


```
#!bash

# 1. Get bitcoinxt
# If you want to make your own changes, the original XT repo is here: git@github.com:bitcoinxt/bitcoinxt.git
git clone git@bitbucket.org:bitcartel/bitcoinxt.git

# 2. Checkout the branch we want
git checkout only-bigblocks
# Optional: If you are making your own changes to the original XT repo, edit, commit and tag e.g. git tag v0.11.0-BIP101

# 3. Get gitian
git clone git@github.com:devrandom/gitian-builder.git

# 4. Install requirements to build with KVM
sudo apt-get install git apache2 apt-cacher-ng python-vm-builder ruby qemu-utils

# 5. Build base image
cd gitian-builder
bin/make-base-vm --arch amd64 --suite precise

# 6. Set environment variables, clear LXC switch in case it was used before.  SIGNER can also be name/word of key.
unset USE_LXC
export SIGNER=73AEB925
export VERSION=0.11.0
export BTCPATH=/home/username/projects/bitcoinxt

# 7. Get input dependencies
mkdir inputs
wget -P inputs https://bitcoincore.org/cfields/osslsigncode-Backports-to-1.7.1.patch
wget -P inputs http://downloads.sourceforge.net/project/osslsigncode/osslsigncode/osslsigncode-1.7.1.tar.gz
make -C ../bitcoinxt/depends download SOURCES_PATH=`pwd`/cache/common

# 9. Build. Will override remote URL in gitian yml file and clone from local bitcoinxt repo. The commit refers to a commit hash or a tag that you want to build.
./bin/gbuild --url bitcoin=${BTCPATH} --commit bitcoin=v${VERSION}-BIP101 ../bitcoinxt/contrib/gitian-descriptors/gitian-linux.yml
mv build/out/bitcoin-*.tar.gz build/out/src/bitcoin-*.tar.gz ../

# 10. Sign and output signature files in parent folder.
./bin/gsign --signer $SIGNER --release ${VERSION}-linux --destination ../gitian.sigs/ ../bitcoinxt/contrib/gitian-descriptors/gitian-linux.yml
```



### Contribute ###

Perform your own Gitian build and submit your key and signature file.